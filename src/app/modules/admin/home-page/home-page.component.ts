import { Component, OnInit } from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';

// Services
import { TokenStorageService } from '@services/token/token-storage.service';
import {GlobalService} from '@services/global.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  residences: JSON[];
  isLoading: boolean = true;

  constructor(
    private global: GlobalService,
    private token: TokenStorageService,
    public modalService: NgxSmartModalService
  ) { }

  ngOnInit(): void {
    this.getResidences();
  }

  getResidences() {
    this.global.getQuery('residence-auditor/' + this.token.getUser().user.id).subscribe(
      res => {
        this.isLoading = false;
        this.residences = res.response;
      }, error => {
        this.isLoading = false;
        console.log(error);
      }
    );
  }

}
