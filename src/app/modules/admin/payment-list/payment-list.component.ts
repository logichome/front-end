import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '@services/global.service';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.scss']
})
export class PaymentListComponent implements OnInit {

  residence: number;
  error: any;
  tableConfig;
  table;
  data;
  isOpened = false;
  isLoading: boolean = true;

  constructor(
    private router: ActivatedRoute,
    private global: GlobalService,
    public ngxSmartModalService: NgxSmartModalService
  ) {
    this.tableConfig = [
      { name: 'transaction_ref', label: 'Referencia', type: '', width: '' },
      { name: 'amount_payed', label: 'Monto', type: '', width: '' },
      { name: 'bcv', label: 'Cambio', type: '', width: '' },
      { name: 'bank.bank', label: 'Banco', type: '', width: '' },
      { name: 'status.name', label: 'Estado', type: '', width: '' },
      { name: 'property.reference', label: 'Propiedad', type: '', width: '' },
      { name: 'property.balance', label: 'Balance', type: '', width: '' },
      { name: 'method.method', label: 'Metodo de pago', type: '', width: '' },
      { name: 'currency.name', label: 'Moneda', type: '', width: '' },
      { name: 'action', label: 'Acción', type: '', width: '', actionText: 'Aprobar' },
    ];
  }

  ngOnInit(): void {
    this.getResidence();
    this.getPaymentsList();
  }

  getResidence() {
    this.router.paramMap.subscribe(
      params => this.residence = parseInt(params.get('id'))
    );
  }

  onAction(event) {
    if (event.action === 'Aprobar') {
      this.data = event.data;
      this.isOpened = true;
      this.ngxSmartModalService.getModal('confirmPayment').open();
    }
  }

  getPaymentsList() {
    this.global.getQuery('unconfirmed-payments/' + this.residence).subscribe(
      res => {
        this.isLoading = false;
        this.table = res.response;
      }, error => {
        this.isLoading = false;
        console.log(error);
      }
    );
  }

}
