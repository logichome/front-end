import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './admin.component';
import {HomePageComponent} from './home-page/home-page.component';
import {ManagerDashboardComponent} from './manager-dashboard/manager-dashboard.component';
import {InvoicesComponent} from '@app/modules/admin/invoices/invoices.component';
import {DefaultersComponent} from '@app/modules/admin/defaulters/defaulters.component';
import {PropertiesComponent} from '@app/modules/admin/properties/properties.component';
import {PaymentListComponent} from '@app/modules/admin/payment-list/payment-list.component';
import {InvoiceDetailComponent} from '@app/modules/admin/invoice-detail/invoice-detail.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        component: HomePageComponent
      },
      {
        path: 'dashboard/:id',
        component: ManagerDashboardComponent
      },
      {
        path: 'invoices/:id',
        component: InvoicesComponent
      },
      {
        path: 'balance/:id',
        component: DefaultersComponent
      },
      {
        path: 'properties/:id',
        component: PropertiesComponent
      },
      {
        path: 'payment-list/:id',
        component: PaymentListComponent
      },
      {
        path: 'invoice/:id/:residence',
        component: InvoiceDetailComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
