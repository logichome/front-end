import { Component, OnInit } from '@angular/core';
import {GlobalService} from '@services/global.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-manager-dashboard',
  templateUrl: './manager-dashboard.component.html',
  styleUrls: ['./manager-dashboard.component.scss']
})
export class ManagerDashboardComponent implements OnInit {

  residence: number;
  table: any[];
  tableContent;
  invoiceStatus: boolean = false;
  currentInvoice;
  isLoading: boolean = true;
  totalSpend: number = 0;
  response;
  error: boolean = false;
  isFirstTime: Boolean;
  content: string;

  constructor(
    private global: GlobalService,
    private router: ActivatedRoute
  ) {
    this.table = [
      { name: 'name', label: 'Nombre', type: '', width: '' },
      { name: 'reason', label: 'Razón', type: '', width: '' },
      { name: 'spend_date', label: 'Fecha', type: 'date', width: '' },
      { name: 'amount', label: 'Monto', type: '40'},
    ];
  }

  ngOnInit(): void {
    this.getResidence();
    this.getCurrentInvoice();
  }

  refresh() {
    this.table = [
      { name: 'name', label: 'Nombre', type: '', width: '' },
      { name: 'reason', label: 'Razón', type: '', width: '' },
      { name: 'spend_date', label: 'Fecha', type: 'date', width: '' },
      { name: 'amount', label: 'Monto', type: '40'},
    ];;
    this.tableContent = [];
    this.invoiceStatus = false;
    this.currentInvoice = null;
    this.isLoading = true;
    this.totalSpend = 0;
    this.response = null;
    this.error = false;
    this.isFirstTime = null;
    this.content = '';
  }

  getResidence() {
    this.router.paramMap.subscribe(
      params => this.residence = parseInt(params.get('id'))
    );
  }

  getCurrentInvoice() {
    this.global.getQuery('last-invoice/' + this.residence).subscribe(
      res => {
        if (res.response[0].invoices.length > 0) {
          this.content = 'Pagina de inicio';
          this.currentInvoice = res.response[0].invoices[0].id;
          this.invoiceStatus = true;
          if (res.response[0].invoices[0].charges.length > 0) {
            this.totalSpend = res.response[0].invoices[0].charges.map(spend => spend.amount)
              .reduce((acc, spend) => spend + acc);
            this.isFirstTime = false;
            this.tableContent = res.response[0].invoices[0].charges;
          }

        } else if (res.response[0].invoices.length == 0) {
          this.tableContent = null;
          this.isFirstTime = true;
          this.invoiceStatus = false;
          this.content = 'Antes de empezar a crear un recibo, debes crear propiedades dentro del' +
            ' condominio, una vez tengas las propiedades listas, podrás crear tu primer recibo';
        }
        this.response = res.response[0];
        this.isLoading = false;
        this.global.pushData('current_invoice', res.response[0]);
        this.error = false;
      }, error => {
        this.error = true;
        this.isFirstTime = true;
        this.isLoading = false;
        this.totalSpend = 0;
        this.response = null;
        this.currentInvoice = null;
        this.invoiceStatus = false;
        this.tableContent = null;
        console.log(error);
      }
    );
  }

}
