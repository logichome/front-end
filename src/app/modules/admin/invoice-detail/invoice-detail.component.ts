import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '@services/global.service';
import {$e} from 'codelyzer/angular/styles/chars';

@Component({
  selector: 'app-invoice-detail',
  templateUrl: './invoice-detail.component.html',
  styleUrls: ['./invoice-detail.component.scss']
})
export class InvoiceDetailComponent implements OnInit {

  invoice: number;
  residence: number;
  residenceArray;
  tables: string[] = ['Titulo', 'Razon', 'Fecha', 'Monto total'];
  tablesSpecific: string[] = ['Titulo', 'Fecha', 'Monto total'];
  tableContentNormal: JSON[] = [];
  tableContentNoNormal: JSON[] = [];
  tableContentSpecific: JSON[] = [];
  allSpends;
  dataInvoice;
  totalInvoice = 0;
  totalSpecific = 0;
  loading: boolean = true;
  error: boolean = false;
  message: string;
  properties;
  property = -1;
  userData;
  isPaid;
  alicuota;
  total;
  reserve;

  constructor(
    private router: ActivatedRoute,
    private global: GlobalService
  ) { }

  ngOnInit(): void {
    this.getResidenceInvoice();
    this.getInvoice();
    this.getProperties();
  }

  getResidenceInvoice() {
    this.router.paramMap.subscribe(
      params => {
        this.invoice = parseInt(params.get('id'));
        this.residence = parseInt(params.get('residence'));
      }
    );
  }

  getInvoice() {
    this.global.getQuery('invoice/' + this.invoice).subscribe(
      res => {
        this.dataInvoice = res.response.invoice;
        this.residenceArray = res.response.residence;
        res.response.invoice.charges.forEach(
          element => {
            if (element.type === 1) {
              this.tableContentNormal.push(element);
              this.totalInvoice += element.amount;
            }
            if (element.type === 2) {
              this.tableContentNoNormal.push(element);
              this.totalInvoice += element.amount;
            }
            if (element.type === 3) {
              this.tableContentSpecific.push(element);
              this.totalSpecific += element.amount;
            }
          }
        );
        this.allSpends = res.response.invoice.charges;
      }, error => {
        console.log(error);
        this.error = true;
        this.loading = false;
        this.message = 'Ups, no se ha podido cargar los gastos, por favor vuelva a intentarlo';
      }
    );
  }

  getProperties() {
    this.global.getQuery('property-residence/' + this.residence).subscribe(
      res => {
        this.properties = res.response;
        this.error = false;
        this.loading = false;
      }, error => {
        this.error = true;
        this.loading = false;
        this.message = 'Ups, no se ha podido cargar las propiedades, vuelva a intentarlo';
      }
    );
  }

  checkProperty($event) {
    this.totalSpecific = 0;
    this.totalInvoice = 0;
    if (this.tables[this.tables.length - 1] !== 'Alicuota') {
      this.tables.push('Participación');
    }
    this.userData = $event;
    this.isInvoicePaid($event.id);
    this.allSpends.forEach( (e, i) => {
      if (e.propertyId === $event.id) {
        this.totalSpecific += e.amount;
      }
      if (e.propertyId === null) {
        this.totalInvoice += e.amount;
      }
    });
    this.alicuota = (this.totalInvoice * $event.alicuota) / 100;
    this.reserve = (this.alicuota * this.residenceArray.reserve_percentage / 100);
    this.total = this.reserve + this.alicuota + this.totalSpecific;
  }

  isInvoicePaid(propertyId) {
    this.global.getQuery('checkIfPayed/' + this.invoice + '/' + propertyId).subscribe(
      res => {
        this.isPaid = res.response[0];
        console.log(res);
      }, error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  printDiv() {
    window.print();
  }

}
