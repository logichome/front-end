import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '@services/global.service';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss']
})
export class PropertiesComponent implements OnInit {

  residence: number;
  data;
  error: boolean;
  message: string;
  table;
  isLoading: boolean = true;

  constructor(
    private router: ActivatedRoute,
    private global: GlobalService
  ) {
    this.table = [
      { name: 'reference', label: 'Codigo referencial', type: '', width: '' },
      { name: 'user.first_name', label: 'Nombre', type: 'date', width: '' },
      { name: 'alicuota', label: 'Alicuota', type: '40',},
    ];
  }

  ngOnInit(): void {
    this.getResidence();
    this.getProperties()
  }

  getResidence() {
    this.router.paramMap.subscribe(
      params => this.residence = parseInt(params.get('id'))
    );
  }

  getProperties() {
    this.global.getQuery('property-residence/' + this.residence).subscribe(
      res => {
        this.data = res.response;
        this.isLoading = false;
      }, error => {
        this.error = true;
        this.isLoading = false;
        this.message = 'Ups, ha sucedido algo, por favor vuelva a intentarlo.'
      }
    );
  }

  onAction(event) {
    console.log(event);
  }

}
