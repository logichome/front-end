import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '@services/token/token-storage.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(
    public routerNav: Router,
    private router: ActivatedRoute,
    private token: TokenStorageService,
  ) { }

  ngOnInit(): void {
    if (this.token.getUser() === null || this.token.getUser() === undefined) {
      this.routerNav.navigate(['/auth/login'])
    }
    if (this.token.getUser().user.type_id === 3) {
      this.routerNav.navigate(['/']);
    }
  }

}
