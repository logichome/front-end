import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '@services/global.service';

@Component({
  selector: 'app-defaulters',
  templateUrl: './defaulters.component.html',
  styleUrls: ['./defaulters.component.scss']
})
export class DefaultersComponent implements OnInit {

  residence: number;
  invoiceStatus: boolean;
  table: any[];
  tableContent;
  error: boolean;
  message: string;
  balance;
  debt: number = 0;
  subscriber: number = 0;
  isLoading: boolean = false;
  response: any;

  constructor(
    private router: ActivatedRoute,
    private  global: GlobalService
  ) {
    this.table = [
      { name: 'reference', label: 'Referencia', type: '', width: '' },
      { name: 'alicuota', label: 'Alicuota', type: '', width: '' },
      { name: 'balance', label: 'Balance por propiedad', type: '', width: '' },
    ];
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.getResidence();
    this.getBalance();
  }

  getResidence() {
    this.router.paramMap.subscribe(
      params => this.residence = parseInt(params.get('id'))
    );
  }

  getBalance() {
    this.global.getQuery('balance' + this.residence).subscribe(
      res => {
        this.response = res.response;
        this.balance = res.response.properties.map(property => property.balance)
          .reduce((acc, balance) => balance + acc);
        res.response.properties.forEach( (value, i) => {
          res.response.properties[i].balance *= -1;
          if (value.balance < 0) {
            this.subscriber += value.balance;
          } else if(value.balance > 0) {
            this.debt += value.balance;
          }
        });
        this.tableContent = res.response.properties;
        this.isLoading = false;
        this.error = false;
      }, error => {
        this.isLoading = false;
        this.error = true;
        this.message = 'Ups, ha sucedido algo, por favor vuelva a intentarlo.';
      }
    );
  }

}
