//* Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//* UI
import { NavbarModule } from '@ui/general/navbar/navbar.module';
import { SidebarModule } from '@ui/admin/sidebar/sidebar.module';
import { CardsModule } from '@ui/general/cards/cards.module';
import { TableModule } from '@ui/general/table/table.module';

//* Components
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';
import {AdminModalModule} from '@ui/admin/admin-modal/admin-modal.module';
import { InvoicesComponent } from './invoices/invoices.component';
import { DefaultersComponent } from './defaulters/defaulters.component';
import { PropertiesComponent } from './properties/properties.component';
import { PaymentListComponent } from './payment-list/payment-list.component';
import { InvoiceDetailComponent } from './invoice-detail/invoice-detail.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [AdminComponent, HomePageComponent, ManagerDashboardComponent, InvoicesComponent, DefaultersComponent, PropertiesComponent, PaymentListComponent, InvoiceDetailComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    NavbarModule,
    SidebarModule,
    CardsModule,
    TableModule,
    AdminModalModule,
    FormsModule,
  ],
})
export class AdminModule {}
