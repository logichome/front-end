import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GlobalService} from '@services/global.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {

  residence: number;
  invoiceStatus: boolean;
  error: boolean;
  message: string;
  data: any[];
  table: any[];
  isLoading: boolean = true;

  constructor(
    private router: ActivatedRoute,
    private global: GlobalService,
    private route: Router
  ) {
    this.table = [
      { name: 'ref_code', label: 'Codigo referencial', type: '', width: '' },
      { name: 'date', label: 'Fecha', type: 'date', width: '' },
      { name: 'total', label: 'Monto total', type: '40',},
      { name: 'action', label: 'Acción', type: '', width: '', actionText: 'Ver' },
    ];
  }

  ngOnInit(): void {
    this.getResidence();
    this.getInvoices();
  }

  getResidence() {
    this.router.paramMap.subscribe(
      params => this.residence = parseInt(params.get('id'))
    );
  }

  onAction(event) {
    if (event.action === 'Ver') {
      this.route.navigate(['manager/invoice/' + event.data.id + '/' + this.residence]);
    }
  }

  getInvoices() {
    this.global.getQuery('invoice-residence/' + this.residence).subscribe(
      res => {
        this.isLoading = false;
        this.data = res.response;
      }, error => {
        this.error = true;
        this.isLoading = false;
        this.message = 'Ups, ha sucedido algo, por favor vuelva a intentarlo.'
      }
    );
  }

}
