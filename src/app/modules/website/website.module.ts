//* Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//* UI
import { TableModule } from '@ui/general/table/table.module';
import { CardsModule } from '@ui/general/cards/cards.module';
import { NavbarModule } from '@ui/general/navbar/navbar.module';
import { SidebarPanelModule } from '@ui/user/sidebar-panel/sidebar-panel.module';
import { AdminModalModule } from '@ui/admin/admin-modal/admin-modal.module';

//* Website
import { WebsiteRoutingModule } from './website-routing.module';
import { WebsiteComponent } from './website.component';
import { HomePageComponent } from './home-page/home-page.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DefaultersComponent } from './defaulters/defaulters.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { MyBalanceComponent } from './my-balance/my-balance.component';
import { InvoiceDetailComponent } from './invoice-detail/invoice-detail.component';

@NgModule({
  declarations: [
    WebsiteComponent,
    HomePageComponent,
    DashboardComponent,
    DefaultersComponent,
    InvoicesComponent,
    MyBalanceComponent,
    InvoiceDetailComponent,
  ],
  imports: [
    CommonModule,
    WebsiteRoutingModule,
    CardsModule,
    NavbarModule,
    SidebarPanelModule,
    TableModule,
    AdminModalModule,
  ],
})
export class WebsiteModule {}
