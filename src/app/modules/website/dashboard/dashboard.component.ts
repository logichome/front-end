import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '@services/global.service';
import { TokenStorageService } from '@services/token/token-storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  paramId;
  table: any[];
  tableContent;
  isLoading: boolean = false;
  totalSpend = 0;
  response;
  error: boolean = true;
  content: string;

  constructor(
    public routerNav: Router,
    private router: ActivatedRoute,
    private token: TokenStorageService,
    private global: GlobalService,
  ) {
    this.table = [
      { name: 'name', label: 'Nombre', type: '', width: '' },
      { name: 'reason', label: 'Razón', type: '', width: '' },
      { name: 'spend_date', label: 'Fecha', type: 'date', width: '' },
      { name: 'amount', label: 'Monto', type: '40'},
    ];
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.getRouter();
    this.getInfo();
  }

  getInfo() {
    this.global.getQuery('last-invoice/' + this.paramId).subscribe(
      res => {
        this.response = res.response[0];
        this.isLoading = false;
        this.global.pushData('current_invoice', res.response[0]);

        if (res.response[0].invoices.length > 0) {
          if (res.response[0].invoices[0].charges.length > 0) {
            this.totalSpend = res.response[0].invoices[0].charges.map(spend => spend.amount)
              .reduce((acc, spend) => spend + acc);
            this.tableContent = res.response[0].invoices[0].charges;
            this.content = 'Estos son los ultimos gastos del mes.';
          }
        } else if (res.response[0].invoices.length == 0) {
          this.content = 'Aún no se ha creado el primer recibo, por favor, espere.';
        }

        this.error = false;
      }, error => {
        this.error = true;
        this.isLoading = false;
        this.totalSpend = 0;
        this.response = null;
        this.tableContent = null;
      }
    );
  }

  getRouter() {
    this.router.paramMap.subscribe(
      params => {
        this.paramId = params.get('id');
      }
    );
  }

}
