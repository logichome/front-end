import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '@services/global.service';
import {element} from 'protractor';

@Component({
  selector: 'app-invoice-detail',
  templateUrl: './invoice-detail.component.html',
  styleUrls: ['./invoice-detail.component.scss']
})
export class InvoiceDetailComponent implements OnInit {

  residence: number;
  invoice: number;
  tables: string[] = ['Titulo', 'Razon', 'Fecha', 'Alicuota', 'Monto total'];
  tableContentNormal: JSON[] = [];
  tableContentNoNormal: JSON[] = [];
  tableContentSpecific: JSON[] = [];
  dataResidence;
  dataInvoice;
  totalInvoice = 0;
  loading: boolean;
  property: any;
  isPaid: boolean;
  propertyLength;
  alicuotaPay;
  total;
  propertySpend = 0;

  constructor(
    private router: ActivatedRoute,
    public global: GlobalService
  ) { }

  ngOnInit(): void {
    this.property = this.global.getData('property');
    this.loading = true;
    this.getResidenceInvoice();
    this.getInvoice();
    this.isInvoicePaid();
  }

  getResidenceInvoice() {
    this.router.paramMap.subscribe(
      params => {
        this.residence = parseInt(params.get('id'));
        this.invoice = parseInt(params.get('invoice'))
      }
    );
  }

  getInvoice() {
    this.global.getQuery('invoice/' + this.invoice).subscribe(
      res => {
        this.dataInvoice = res.response.invoice;
        this.dataResidence = res.response.residence;
        this.dataInvoice.charges.forEach(
          element => {
            if (element.propertyId == null) {
              element.alicuota = (element.amount * this.property.alicuota) / 100;
              this.totalInvoice += element.amount;
            }
            if (element.propertyId == this.property.id) {
              this.tableContentSpecific.push(element);
              this.propertySpend += element.amount;
            }
            if (element.type === 1) {
              this.tableContentNormal.push(element);
            }
            if (element.type === 2) {
              this.tableContentNoNormal.push(element);
            }
          });
        this.alicuotaPay = (this.totalInvoice * this.global.getData('property').alicuota) / 100;
        this.total = (this.alicuotaPay * this.dataResidence.reserve_percentage / 100) + this.alicuotaPay;
      }, error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  isInvoicePaid() {
    this.global.getQuery('checkIfPayed/' + this.invoice + '/' + this.property.id).subscribe(
      res => {
        this.loading = false;

        this.isPaid = res.response;
      }, error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

}
