import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '@services/token/token-storage.service';

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.scss']
})
export class WebsiteComponent implements OnInit {

  token;

  constructor(
    public routerNav: Router,
    private router: ActivatedRoute,
    private _token: TokenStorageService
  ) { }

  ngOnInit(): void {
    if (this._token.getUser() === null) {
      this.routerNav.navigate(['/auth/login'])
    } else if (this._token.getUser().user.type_id === 2) {
      this.routerNav.navigate(['/manager']);
    }
  }


 }
