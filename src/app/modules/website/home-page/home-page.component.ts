import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Services
import {GlobalService} from '@services/global.service';
import { TokenStorageService } from '@services/token/token-storage.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  properties: JSON[];
  isLoading: boolean = true;
  error: boolean = false;
  message: string;

  constructor(
    public router: Router,
    private global: GlobalService,
    private token: TokenStorageService
  ) { }

  ngOnInit(): void {
    if (this.token.getUser() === null) {
       this.router.navigate(['/auth/login'])
    } else if (this.token.getUser().user.type_id === 3) {
        this.router.navigate(['/']);
        this.getProperties();
    } else if (this.token.getUser().user.type_id === 2) {
        this.router.navigate(['/manager']);
    }
   }

  getProperties() {
    this.global.getQuery('property-user/' + this.token.getUser().user.id).subscribe(
      res => {
        this.isLoading = false;
        this.properties = res;
        if (res.length <= 0) {
          this.message = 'Aún no tienes propiedades registradas en el sistema.';
        }
      }, error => {
        this.error = true;
        this.isLoading = false;
        console.log(error);
      }
    );
  }

  setPropertyId(id) {
    this.global.pushData('property', id);
  }

}
