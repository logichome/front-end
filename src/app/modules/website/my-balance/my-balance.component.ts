import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '@services/global.service';

@Component({
  selector: 'app-my-balance',
  templateUrl: './my-balance.component.html',
  styleUrls: ['./my-balance.component.scss']
})
export class MyBalanceComponent implements OnInit {

  residence: number;
  property: any;
  paymentInfo: any;
  payments: JSON[];
  config: any[];
  isLoading: boolean;
  error: boolean;

  constructor(
    private router: ActivatedRoute,
    private global: GlobalService
  ) {
    this.config = [
      { name: 'transaction_ref', label: 'Numero de transaccion', type: '', width: '' },
      { name: 'amount_payed', label: 'Monto pagado', type: '', width: '' },
      { name: 'bcv', label: 'Tasa del BCV', type: '', width: '' },
    ];
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.getResidence();
    this.getPaymentInfo();
  }

  getResidence() {
    this.router.paramMap.subscribe(
      params => {
        this.residence = parseInt(params.get('id'));
        this.property = this.global.getData('property');
      }
    );
  }


  getPaymentInfo() {
    this.global.getQuery('property-payment/' + this.property.id).subscribe(
      res => {
        this.paymentInfo = res.response[0];
        this.payments = res.response[0].payments;
        this.isLoading = false;
        this.error = false;
      }, error => {
        this.isLoading = false;
        this.error = true;
        console.log(error);
      }
    );
  }

}
