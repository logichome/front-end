import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WebsiteComponent} from './website.component';
import {HomePageComponent} from './home-page/home-page.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {InvoicesComponent} from './invoices/invoices.component';
import {DefaultersComponent} from './defaulters/defaulters.component';
import {MyBalanceComponent} from './my-balance/my-balance.component';
import {InvoiceDetailComponent} from '@app/modules/website/invoice-detail/invoice-detail.component';

const routes: Routes = [
  {
    path: '',
    component: WebsiteComponent,
    children: [
      {
        path: '',
        component: HomePageComponent
      },
      {
        path: 'dashboard/:id',
        component: DashboardComponent
      },
      {
        path: 'invoices/:id',
        component: InvoicesComponent
      },
      {
        path: 'defaulters/:id',
        component: DefaultersComponent
      },
      {
        path: 'my-balance/:id',
        component: MyBalanceComponent
      },
      {
        path: 'invoice/:id/:invoice',
        component: InvoiceDetailComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebsiteRoutingModule { }
