import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '@services/token/token-storage.service';
import {AuthService} from '@services/auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GlobalService} from '@services/global.service';

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss']
})
export class InvitationComponent implements OnInit {

  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  residence: {};
  user: any;

  constructor(private token: TokenStorageService,
              private auth: AuthService,
              private global: GlobalService,
              private activatedRoute: ActivatedRoute,
              public router: Router
            ) { }

  ngOnInit(): void {
    this.getParams();
  }

  onSubmit(): void {
    this.form['email'] = this.user;
    this.auth.userRegister(this.form).subscribe(
      (data) => {
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.global.postQuery('property', this.residence).subscribe( res => {
          this.router.navigate(['/auth/login']);
        }, error => {
          this.errorMessage = error.errorMessage;
          this.isSignUpFailed = true;
        });
      },
      (err) => {
        this.errorMessage = err.errorMessage;
        this.isSignUpFailed = true;
      },
    );
  }

  getParams() {
    this.activatedRoute.queryParams.subscribe( params => {
      this.user = params.email;
      this.residence = {
        balance: params.balance,
        alicuota: params.alicuota,
        reference: params.reference,
        residence_id: params.residence,
        user_email: params.email
      };
    });
  }

}
