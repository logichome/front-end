import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TokenStorageService } from '@services/token/token-storage.service';
import { AuthService } from '@services/auth/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private token: TokenStorageService, private auth: AuthService, public router: Router) {}

  ngOnInit(): void {
    if (this.token.getUser().user.type_id === 3) {
      this.router.navigate(['/']);
    } else if (this.token.getUser().user.type_id === 2) {
      this.router.navigate(['/manager']);
    } else {
      this.router.navigate(['/auth/login']);
    }
  }

  onSubmit(): void {
    this.auth.userRegister(this.form).subscribe(
      (data) => {
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.router.navigate(['/auth/login']);
      },
      (err) => {
        this.errorMessage = err.errorMessage;
        this.isSignUpFailed = true;
      },
    );
  }
}
