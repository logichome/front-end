import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditorRegisterComponent } from './auditor-register.component';

describe('AuditorRegisterComponent', () => {
  let component: AuditorRegisterComponent;
  let fixture: ComponentFixture<AuditorRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuditorRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditorRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
