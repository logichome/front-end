//* Angular
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

//* Services
import { AuthService } from '@services/auth/auth.service';
import { TokenStorageService } from '@services/token/token-storage.service';
import { GlobalService } from '@services/global.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  form: any = {};
  isLogedIn = false;
  isLoginFailed = false;
  errorMessage = '';

  constructor(
    public router: Router,
    private authService: AuthService,
    private token: TokenStorageService,
    private global: GlobalService
  ) { }

  ngOnInit(): void {
    if (this.token.getUser().user.type_id === 3) {
      this.router.navigate(['/']);
      this.isLogedIn = true;
    } else if (this.token.getUser().user.type_id === 2) {
      this.router.navigate(['/manager']);
      this.isLogedIn = true;
    } else {
      this.router.navigate(['auth/login'])
      this.isLogedIn = false;
     }
  }

  login(): void {
    this.authService.login(this.form).subscribe(
      data => {
        data.user.type_id = parseInt(data.user.type_id);
        this.token.saveUser(data);
        this.isLoginFailed = false;
        this.isLogedIn = true;
        this.global.pushData('user', data);
        if (this.token.getUser().user.type_id === 3) {
          this.router.navigate(['/']);
        } else if (this.token.getUser().user.type_id === 2) {
          this.router.navigate(['manager']);
        } else {
          this.router.navigate(['/auth/register'])
        }
      },
       error => {
         this.errorMessage = error.errorMessage;
         this.isLoginFailed = true;
         console.log(error);
      }
    );
  }

}

