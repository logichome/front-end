//* Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

//* Routes
import { routes } from './auth-routing.module';

//* Modules
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

import { authInterceptorProviders } from '@services/interceptor/auth-interceptor.service';
import { AuditorRegisterComponent } from './auditor-register/auditor-register.component';
import { InvitationComponent } from './invitation/invitation.component';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, AuditorRegisterComponent, InvitationComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    FormsModule,
  ],
  providers: [authInterceptorProviders]
})
export class AuthModule {}
