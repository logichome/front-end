import { Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuditorRegisterComponent } from './auditor-register/auditor-register.component';
import { InvitationComponent } from '@app/modules/auth/invitation/invitation.component';

export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'auditor-register',
    component: AuditorRegisterComponent
  },
  {
    path: 'invitation',
    component: InvitationComponent
  }
];
