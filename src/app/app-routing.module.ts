import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => {
      return import('./modules/website/website.module').then( m => m.WebsiteModule);
    }
  },
  {
    path: 'auth',
    loadChildren: () => {
      return import('./modules/auth/auth.module').then( m => m.AuthModule);
    }
  },  {
    path: 'manager',
    loadChildren: () => {
      return import('./modules/admin/admin.module').then( m => m.AdminModule);
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
