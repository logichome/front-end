import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  data: {} = {};

  constructor(
    private http: HttpClient
  ) { }

 pushData(key: string, data: any) {
    this.data[key] = data;
  }

  getData(key) {
    return this.data[key];
  }

  getQuery(route: string, header?: JSON): Observable<any> {
    return this.http.get(environment.endpoint + route);
  }

  postQuery(route: string, data: any, header?: JSON): Observable<any> {
    return this.http.post(environment.endpoint + route, data);
  }

  getQueryForExternals(route: string, header?: JSON): Observable<any> {
    return this.http.get(route);
  }
}
