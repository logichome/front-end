//* Angular
import { Injectable } from '@angular/core';
import {
  HTTP_INTERCEPTORS,
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';

//* Services
import { TokenStorageService } from '@services/token/token-storage.service';

const tokenHeaderKey = 'Authorization';
//const tokenHeaderKey = 'x-acces-token';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private token: TokenStorageService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authRequest = req;
    const token = this.token.getUser();
    if (token != null || token != undefined) {
      authRequest = req.clone({ headers: req.headers.set(tokenHeaderKey, 'Bearer ' + token.access_token)});
      //authRequest = req.clone({ headers: req.headers.set(tokenHeaderKey, token)});
    }
    return next.handle(authRequest);
  }
}

export const authInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptorService,
    multi: true
  }
]
