import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environments/environment';
import {Observable} from 'rxjs';

const tokenKey = 'auth-token';
const userKey = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor(
    private http: HttpClient
  ) { }

  signOut(): void {
    window.localStorage.clear();
  }

  public saveToken(token): void {
    window.localStorage.removeItem(tokenKey);
    window.localStorage.setItem(tokenKey, token);
  }

  public getToken(): any {
    return localStorage.getItem(tokenKey);
  }

  public saveUser(user): void {
    window.localStorage.removeItem(userKey);
    window.localStorage.setItem(userKey, JSON.stringify(user));
  }

  public getUser(): any {
    return JSON.parse(localStorage.getItem(userKey));
  }

}
