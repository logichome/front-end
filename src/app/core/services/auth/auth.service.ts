import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '@environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(credentials): Observable<any> {
    return this.http.post(environment.endpoint + 'auth/login', {
      email: credentials.email,
      password: credentials.password
    }, httpOptions);
  }

  userRegister(user): Observable<any> {
    return this.http.post(environment.endpoint + 'auth/register', {
      first_name: user.first_name,
      last_name: user.last_name,
      type_dni: 'Cedula',
      dni: user.dni,
      email: user.email,
      password: user.password,
      password_confirmation: user.password_confirmation,
      type_id: '3'
    }, httpOptions)
  }

  auditorRegister(user): Observable<any> {
    return this.http.post(environment.endpoint + 'auth/register', {
      first_name: user.first_name,
      last_name: user.last_name,
      type_dni: 'Cedula',
      dni: user.dni,
      email: user.email,
      password: user.password,
      password_confirmation: user.password_confirmation,
      type_id: 2
    }, httpOptions)
  }
}
