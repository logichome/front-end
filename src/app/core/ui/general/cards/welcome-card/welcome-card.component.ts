import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-welcome-card',
  templateUrl: './welcome-card.component.html',
  styleUrls: ['./welcome-card.component.scss']
})
export class WelcomeCardComponent implements OnInit {

  @Input() title: string;
  @Input() content: any;

  constructor() { }

  ngOnInit(): void {
  }

}
