import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElementsCardsComponent } from './elements-cards/elements-cards.component';
import {RouterModule} from '@angular/router';
import { WelcomeCardComponent } from './welcome-card/welcome-card.component';



@NgModule({
  declarations: [ElementsCardsComponent, WelcomeCardComponent],
  exports: [
    ElementsCardsComponent,
    WelcomeCardComponent
  ],
    imports: [
        CommonModule,
        RouterModule
    ]
})
export class CardsModule { }
