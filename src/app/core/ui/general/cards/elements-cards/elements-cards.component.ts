import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-elements-cards',
  templateUrl: './elements-cards.component.html',
  styleUrls: ['./elements-cards.component.scss']
})
export class ElementsCardsComponent implements OnInit {

  @Input() title;
  @Input() content;
  @Input() link;
  @Input() image;
  @Output() event = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  onEvent() {
    this.event.emit();
  }

}
