import {Component, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnChanges {

  @Input() config;
  @Input() data;
  @Output() newData: EventEmitter<boolean> = new EventEmitter()
  @Output() actionEvent: EventEmitter<any> = new EventEmitter()
  public gridData: GridDataResult;
  public state: State = {
    skip: 0,
    take: 20
  };
  constructor() { }

  ngOnChanges() {
    this.gridData = this.data ? process(this.data, this.state) : null
  }

  dataStateChange(state: DataStateChangeEvent) {
    this.state = state;
    this.gridData = process(this.data, this.state);
  }

  action(id, action, data) {

    this.actionEvent.emit({ id: id, action: action, data: data })
  }

}
