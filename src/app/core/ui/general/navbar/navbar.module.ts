import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainNarvbarComponent } from './main-narvbar/main-narvbar.component';
import {RouterModule} from '@angular/router';



@NgModule({
    declarations: [MainNarvbarComponent],
    exports: [
        MainNarvbarComponent
    ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class NavbarModule { }
