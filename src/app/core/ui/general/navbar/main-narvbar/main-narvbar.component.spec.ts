import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainNarvbarComponent } from './main-narvbar.component';

describe('MainNarvbarComponent', () => {
  let component: MainNarvbarComponent;
  let fixture: ComponentFixture<MainNarvbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainNarvbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNarvbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
