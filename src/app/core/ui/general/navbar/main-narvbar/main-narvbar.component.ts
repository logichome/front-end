import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from '@services/token/token-storage.service';

@Component({
  selector: 'app-main-narvbar',
  templateUrl: './main-narvbar.component.html',
  styleUrls: ['./main-narvbar.component.scss']
})
export class MainNarvbarComponent implements OnInit {

  isLogedIn = false;
  @ViewChild('navBurger') navBurger: ElementRef;
  @ViewChild('navMenu') navMenu: ElementRef;

  constructor(
    public router: Router,
    private token: TokenStorageService,
  ) { }

  ngOnInit(): void {
    if (this.token.getUser()) {
      this.isLogedIn = true;
    } else {
      this.isLogedIn = false;
    }
  }

  logOut(): void {
    console.log("Clear token");
    this.token.signOut();
    this.router.navigate(['/auth/login']);
  }

  toggleNavbar() {
    this.navBurger.nativeElement.classList.toggle('is-active');
    this.navMenu.nativeElement.classList.toggle('is-active');
  }

}
