import {Component, Input, OnInit} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {GlobalService} from '@services/global.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() invoiceStatus: boolean;
  @Input() residenceId: number;
  @Input() isDashboard: boolean;

  constructor(
    public modalService: NgxSmartModalService,
  ) { }

  ngOnInit(): void {}

}
