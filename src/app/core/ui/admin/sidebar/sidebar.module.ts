import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar.component';
import {AdminModalModule} from '../admin-modal/admin-modal.module';
import {RouterModule} from '@angular/router';



@NgModule({
    declarations: [SidebarComponent],
    exports: [
        SidebarComponent
    ],
  imports: [
    CommonModule,
    AdminModalModule,
    RouterModule
  ]
})
export class SidebarModule { }
