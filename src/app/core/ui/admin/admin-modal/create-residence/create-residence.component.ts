import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

// Services
import { TokenStorageService } from '@services/token/token-storage.service'
import {GlobalService} from '@services/global.service';

@Component({
  selector: 'app-create-residence',
  templateUrl: './create-residence.component.html',
  styleUrls: ['./create-residence.component.scss']
})
export class CreateResidenceComponent implements OnInit {

  formGroup: FormGroup;
  cities: JSON[];
  countries: JSON[];
  states: JSON[];
  types: JSON[];
  isLoading: boolean = false;
  failedForm: boolean = false;

  constructor(
    private global: GlobalService,
    private token: TokenStorageService,
    private fb: FormBuilder,
    private routes: Router
  ) {
    this.formGroup = this.fb.group({
      name: [],
      nif: [],
      community_type: new FormControl({}, Validators.required),
      country_id: [],
      state_id: [],
      city_id: [],
      location: [],
      street: [],
      zip_code: [],
      auditor_id: this.token.getUser().user.id,
      reserve_percentage: [],
      reserve: 0
    });
  }

  ngOnInit(): void {
    this.getCities();
    this.getCountries();
    this.getStates();
    this.getResidenceTypes();
  }

  onSubmitResidence() {
    this.formGroup.value.auditor_id = this.token.getUser().user.id;
    this.formGroup.value.reserve = 0;
    this.isLoading = true;
    this.global.postQuery('new-residence', this.formGroup.value).subscribe(
      res => {
        this.formGroup.reset();
        this.isLoading = false;
        this.failedForm = false;
        this.routes.navigate(['manager/dashboard/' + res.response[1].id])
      },
      error => {
        console.log(error);
        this.isLoading = false;
        this.failedForm = true;
        console.table(error);
      }
    );
  }

  getCities() {
    this.global.getQuery('cities').subscribe(
      res => {
        this.cities = res.response;
      }, error => {
        console.log(error);
      }
    );
  }

  getCountries() {
    this.global.getQuery('countries').subscribe(
      res => {
        this.countries = res.response;
      }, error => {
        console.log(error);
      }
    );
  }

  getStates() {
    this.global.getQuery('states').subscribe(
      res => {
        this.states = res.response;
      }, error => {
        console.log(error);
      }
    );
  }

  getResidenceTypes() {
    this.global.getQuery('residences-types').subscribe(
      res => {
        this.types = res.response;
      }, error => {
        console.log(error);
      }
    );
  }

}
