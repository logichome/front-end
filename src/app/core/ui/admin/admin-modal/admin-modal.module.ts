import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateInvoiceComponent } from './create-invoice/create-invoice.component';
import {NgxSmartModalModule} from 'ngx-smart-modal';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CreateChargesComponent } from './create-charges/create-charges.component';
import { CreateResidenceComponent } from './create-residence/create-residence.component';
import { CreatePropertiesComponent } from './create-properties/create-properties.component';
import { ConfirmPaymentComponent } from './confirm-payment/confirm-payment.component';
import { PersonalChargesComponent } from './personal-charges/personal-charges.component';
import { PaymentsReportComponent } from './payments-report/payments-report.component';
import { DividedChargesComponent } from './divided-charges/divided-charges.component';
import { ProntoPagoComponent } from './pronto-pago/pronto-pago.component';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';



@NgModule({
  declarations: [CreateInvoiceComponent, CreateChargesComponent, CreateResidenceComponent, CreatePropertiesComponent, ConfirmPaymentComponent, PersonalChargesComponent, PaymentsReportComponent, DividedChargesComponent, ProntoPagoComponent],
    exports: [CreateInvoiceComponent, CreateChargesComponent, CreateResidenceComponent, CreatePropertiesComponent, ConfirmPaymentComponent, PersonalChargesComponent, PaymentsReportComponent, DividedChargesComponent, ProntoPagoComponent],
    imports: [
        CommonModule,
        NgxSmartModalModule.forChild(),
        FormsModule,
        ReactiveFormsModule,
        DropDownsModule
    ]
})
export class AdminModalModule { }
