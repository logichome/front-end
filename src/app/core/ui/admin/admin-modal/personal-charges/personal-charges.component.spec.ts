import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalChargesComponent } from './personal-charges.component';

describe('PersonalChargesComponent', () => {
  let component: PersonalChargesComponent;
  let fixture: ComponentFixture<PersonalChargesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalChargesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
