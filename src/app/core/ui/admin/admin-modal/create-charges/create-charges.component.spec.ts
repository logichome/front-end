import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateChargesComponent } from './create-charges.component';

describe('CreateChargesComponent', () => {
  let component: CreateChargesComponent;
  let fixture: ComponentFixture<CreateChargesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateChargesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
