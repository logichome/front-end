import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GlobalService} from '@services/global.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.scss']
})
export class CreateInvoiceComponent implements OnInit {

  currencies: JSON[];
  currentResidence;
  form: FormGroup;
  message: string;
  error: boolean;
  @Output() residenceEvent = new EventEmitter<any>();
  @Input() isFirst;
  @Input() currentInvoice;
  isLoading: boolean = false;

  constructor(
    private global: GlobalService,
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.getCurrencies();
    this.getParams();
    this.form = this.fb.group({
      ref_code: [],
      date: [],
      currency: [],
      residence_id: this.currentResidence,
      status_id: 1,
    });
  }

  getCurrencies() {
    this.global.getQuery('currencies').subscribe(
      res => {
        this.currencies = res.response;
    }, error => console.log(error));
  }

  getParams() {
    this.activeRoute.paramMap.subscribe(
      params =>{
        this.currentResidence = params.get('id');
      }
    );
  }

  onSubmit() {
    this.form.value.residence_id = this.currentResidence;
    this.form.value.status_id = 1;
    this.isLoading = true;
    this.global.postQuery('invoice', this.form.value).subscribe(
      res => {
        this.form.reset();
        if (this.isFirst) {
          this.closeInvoice();
        } else if (!this.isFirst){
          this.isLoading = false;
          this.residenceEvent.emit(true);
          this.message = 'Se ha creado exitosamente la factura';
          this.error = false;
        }
      },
      error => {
        this.isLoading = false;
        console.log(error);
        this.message = 'Ups, ha sucedido algo';
        this.error = true;
        this.residenceEvent.emit(false);
      }
    );
  }

  closeInvoice() {
    this.global.getQuery('turnOffInvoice' + this.currentInvoice).subscribe(
      res => {
        this.isLoading = false;
        this.residenceEvent.emit(true);
        this.message = 'Se ha creado exitosamente la factura';
        this.error = false;
      }, error1 => {
        this.isLoading = false;
        console.log(error1);
        this.message = 'Ups, ha sucedido algo';
        this.error = true;
        this.residenceEvent.emit(false);
      }
    );
  }

}
