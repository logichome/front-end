import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GlobalService} from '@services/global.service';

@Component({
  selector: 'app-confirm-payment',
  templateUrl: './confirm-payment.component.html',
  styleUrls: ['./confirm-payment.component.scss']
})
export class ConfirmPaymentComponent implements OnInit {

  @Input() data;
  @Input() isOpened = false;
  @Output() refresh = new EventEmitter<boolean>();
  isLoading: boolean = false;

  constructor(
    private global: GlobalService
  ) { }

  ngOnInit(): void {
  }

  onClosing() {
    this.data = null;
    this.isOpened = false;
    this.isLoading = false;
  }

  paymentConfirm() {
    this.isLoading = true;
    this.global.getQuery('confirmPayment' + this.data.id).subscribe(
      res => {
        this.isLoading = false
        window.location.reload();
      }, error => {
        this.isLoading = false;
        console.log(error);
      }
    );
  }

}
