import {Component, Input, OnInit} from '@angular/core';
import {GlobalService} from '@services/global.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-create-properties',
  templateUrl: './create-properties.component.html',
  styleUrls: ['./create-properties.component.scss']
})
export class CreatePropertiesComponent implements OnInit {

  form: FormGroup;
  residenceId;
  message: string;
  error: boolean;
  isLoading: boolean = false;
  errType: any = {};

  constructor(
    private global: GlobalService,
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.getParams();
    this.form = this.fb.group({
      user_email: [],
      reference: [],
      alicuota: [],
      residence_id: this.residenceId,
      balance: 0,
    });
  }

  getParams() {
    this.activeRoute.paramMap.subscribe(
      params =>{
        this.residenceId = params.get('id');
      }
    );
  }

  onSubmit() {
    this.form.value.residence_id = this.residenceId;
    this.isLoading = true;
    if (this.form.value.balance == [] || this.form.value.balance == null) {
      this.form.value.balance = 0;
    }
    if (this.form.valid) {
      this.global.postQuery('property', this.form.value).subscribe(
        res => {
          this.isLoading = false;
          this.error = false;
          this.message = 'Has añadido exitosamente una nueva propiedad al condominio.';
          this.form.reset();
        }, error1 => {
          this.isLoading = false;
          this.error = true;
          this.message = 'Ups! Ah sucedido algo, por favor vuelve a intentarlo mas tarde.';
          this.errType = error1;
          console.table(error1);
        }
      );
    }
    if (!this.form.valid) {
      this.error = true;
      this.isLoading = false;
      this.message = 'Has llenado de forma incorrecta el formulario, por favor, completelo y vuelva a intentarlo.'
    }
  }

  sendInvitation() {
    this.isLoading = true;
    if (this.form.value.balance == [] || this.form.value.balance == null) {
      this.form.value.balance = 0;
    }
    this.form.value['email'] = this.form.value.user_email;
    if (this.form.valid) {
      this.global.postQuery('invitation', this.form.value).subscribe(res => {
          this.isLoading = false;
          this.error = false;
          this.form.reset();
          this.message = 'Se ha invitado de forma correcta al correo ' + this.form.value.user_email;
        }, error => {
          this.isLoading = false;
          this.error = true;
          this.errType = error;
          this.message = 'Ups! Ah sucedido algo, por favor vuelve a intentarlo mas tarde.';
          console.log(error);
        }
      );
    }
    if (!this.form.valid) {
      this.error = true;
      this.isLoading = false;
      this.message = 'Has llenado de forma incorrecta el formulario, por favor, completelo y vuelva a intentarlo.'
    }
  }

}
