import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {GlobalService} from '@services/global.service';

@Component({
  selector: 'app-pronto-pago',
  templateUrl: './pronto-pago.component.html',
  styleUrls: ['./pronto-pago.component.scss']
})
export class ProntoPagoComponent implements OnInit {

  @Input() invoice;
  @Input() residence;

  pageLoader: boolean = true;
  form: FormGroup;
  error: boolean;
  message: string;
  isLoading: boolean = false;
  dolar: string;
  @Output() prontoPagoEvent = new EventEmitter<any>();

  constructor(private fb: FormBuilder, private global: GlobalService) { }

  ngOnInit(): void {
    this.getBcv();
  }

  getBcv() {
    this.global.getQuery('bcv-price').subscribe(
      (res) => {
        this.dolar = res.response;
        this.pageLoader = false;
        this.settingUpForm();
      },
      (error1) => {
        console.log(error1);
        this.pageLoader = false;
      },
    );
  }

  ngOnSubmit(): void {
    this.isLoading = true;
    this.form.value.invoice_id = this.invoice;
    this.form.value.dolar = this.dolar;
    this.form.value.residence_id = this.residence;
    this.error = null;
    this.message = null;
    this.isLoading = false;
    this.global.postQuery('prontopago', this.form.value).subscribe(res => {
      this.form.reset();
      this.isLoading = false;
      this.error = false;
      this.message = 'Se ha creado el ProntoPago exitosamente';
      this.prontoPagoEvent.emit(true);
    }, error1 => {
      this.isLoading = false;
      this.error = true;
      this.prontoPagoEvent.emit(false);
      console.log(error1);
    });
  }

  settingUpForm(): void {
    this.form = this.fb.group({
      invoice_id: this.invoice,
      bcv: this.dolar,
      command_date: [],
      percentage_prontopago: [],
      residence_id: this.residence,
    });
  }

}
