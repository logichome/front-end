import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DividedChargesComponent } from './divided-charges.component';

describe('DividedChargesComponent', () => {
  let component: DividedChargesComponent;
  let fixture: ComponentFixture<DividedChargesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DividedChargesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DividedChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
