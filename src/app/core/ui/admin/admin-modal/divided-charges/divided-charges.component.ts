import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '@services/global.service';
import { DropDownFilterSettings } from "@progress/kendo-angular-dropdowns";

@Component({
  selector: 'app-divided-charges',
  templateUrl: './divided-charges.component.html',
  styleUrls: ['./divided-charges.component.scss']
})
export class DividedChargesComponent implements OnInit {
  chargesTypes: JSON[];
  properties: JSON[];
  form: FormGroup;
  message: string;
  error: boolean;
  onProperty = false;
  @Output() chargeEvent = new EventEmitter<any>();
  isLoading: boolean = false;
  pageLoader = true;
  dolar: number;
  residence: Number;
  data;
  @Input() currency;
  select = -1;
  public filterSettings: DropDownFilterSettings = {
    caseSensitive: false,
    operator: "startsWith",
  };

  constructor(private router: ActivatedRoute, private global: GlobalService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.getBcv();
    this.getResidence();
    this.getProperties();
  }

  ngOnSubmit() {
    this.isLoading = true;
    this.form.value.type = 3;
    this.form.value.invoice_id = this.global.getData('current_invoice').invoices[0].id || 0;
    this.form.value.bcv = this.dolar;
    this.global.postQuery('storePersonalCharges', this.form.value).subscribe(
      (res) => {
        this.form.reset();
        this.getBcv();
        this.isLoading = false;
        this.message = 'Has añadido exitosamente un nuevo gasto.';
        this.error = false;
        this.chargeEvent.emit(true);
      },
      (error1) => {
        this.isLoading = false;
        this.message = 'Ups, ha habido un problema, intentalo de nuevo';
        this.error = true;
        this.chargeEvent.emit(false);
        console.log(error1);
      },
    );
  }

  getBcv() {
    this.global.getQuery('bcv-price').subscribe(
      (res) => {
        this.dolar = res.response;
        this.pageLoader = false;
        this.settingForm();
      },
      (error1) => {
        console.log(error1);
        this.pageLoader = false;
      },
    );
  }

  getResidence() {
    this.router.paramMap.subscribe((params) => (this.residence = parseInt(params.get('id'))));
  }

  getProperties() {
    this.global.getQuery('property-residence/' + this.residence).subscribe(
      (res) => {
        this.properties = res.response;
        this.isLoading = false;
      },
      (error) => {
        this.error = true;
        this.isLoading = false;
        this.message = 'Ups, ha sucedido algo, por favor vuelva a intentarlo.';
      },
    );
  }

  settingForm() {
    this.form = this.fb.group({
      invoice_id: this.global.getData('current_invoice').invoices[0].id || 0,
      name: [],
      amount: [],
      reason: [],
      spend_date: [],
      type: [],
      properties: [],
      bcv: this.dolar,
    });
  }

}
