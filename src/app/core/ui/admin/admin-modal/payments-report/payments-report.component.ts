import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {GlobalService} from '@services/global.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-payments-report',
  templateUrl: './payments-report.component.html',
  styleUrls: ['./payments-report.component.scss']
})
export class PaymentsReportComponent implements OnInit {

  methods: any[];
  banks: any[]
  currencies: JSON[];
  properties: JSON[];
  errorLoading: boolean = false;
  message: string;
  form: FormGroup;
  error: boolean;
  isLoading: boolean = true;
  residence;

  constructor(
    private global: GlobalService,
    private formBuilder: FormBuilder,
    private router: ActivatedRoute
  ) {
    this.form = this.formBuilder.group({
      amount_payed2: -1,
      transaction_ref: [],
      id_method: [],
      bank: [],
      property_id: [],
      currency_id: [],
      status: 2
    });
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.getMethods();
    this.getBanks();
    this.getCurrencies();
    this.getProperties();
  }

  getResidence() {
    this.router.paramMap.subscribe((params) => (this.residence = parseInt(params.get('id'))));
  }

  getMethods() {
    this.global.getQuery('methods').subscribe(
      res => {
        this.methods = res.response;
        this.errorLoading = false;
      }, error => {
        this.errorLoading = true;
        this.message = 'Ha sucedido algo, por favor vuelve a intentarlo.';
      }
    );
  }

  getProperties() {
    this.getResidence();
    this.global.getQuery('property-residence/' + this.residence).subscribe(
      (res) => {
        this.properties = res.response;
        this.isLoading = false;
      },
      (error) => {
        this.error = true;
        this.isLoading = false;
        this.message = 'Ups, ha sucedido algo, por favor vuelva a intentarlo.';
      },
    );
  }

  getBanks() {
    this.global.getQuery('banks').subscribe(
      res => {
        this.banks = res.response;
        this.errorLoading = false;
      }, error => {
        this.errorLoading = true;
        this.message = 'Ha sucedido algo, por favor vuelve a intentarlo.';
      }
    );
  }

  getCurrencies() {
    this.global.getQuery('currencies').subscribe(
      res => {
        this.currencies = res.response;
        this.errorLoading = false;
      }, error => {
        this.errorLoading = true;
        this.message = 'Ha sucedido algo, por favor vuelve a intentarlo.';
      }
    );
  }

  ngOnSubmit() {
    // Setting up variables
    this.form.value.status = 2;
    this.form.value.amount_payed2 = -1;
    // setting up value
    const value = this.form.value.amount_payed2 * -1;
    this.form.value.amount_payed = value;
    this.global.postQuery('payment-confirmed', this.form.value).subscribe(
      res => {
        this.form.reset();
        this.message= 'Se ha registrado exitosamente el pago, por favor espere que el administrador lo apruebe';
        this.error = false;
      }, error => {
        console.log(error);
        this.error = true;
        this.message = 'Ha sucedido algo, por favor vuelve a intentarlo.';
      }
    );
  }

}
