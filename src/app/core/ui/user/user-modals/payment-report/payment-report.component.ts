import {Component, Input, OnInit} from '@angular/core';
import {GlobalService} from '@services/global.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-payment-report',
  templateUrl: './payment-report.component.html',
  styleUrls: ['./payment-report.component.scss']
})
export class PaymentReportComponent implements OnInit {

  methods: any[];
  banks: any[]
  currencies: JSON[];
  errorLoading: boolean = false;
  message: string;
  form: FormGroup;
  error: boolean;

  constructor(
    private global: GlobalService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      amount_payed: [],
      transaction_ref: [],
      id_method: [],
      bank: [],
      property_id: this.global.getData('property').id,
      currency_id: [],
      status: 1
    });
  }

  ngOnInit(): void {
    this.getMethods();
    this.getBanks();
    this.getCurrencies();
  }

  getMethods() {
    this.global.getQuery('methods/').subscribe(
      res => {
        this.methods = res.response;
        this.errorLoading = false;
      }, error => {
        this.errorLoading = true;
        this.message = 'Ha sucedido algo, por favor vuelve a intentarlo.';
      }
    );
  }

  getBanks() {
    this.global.getQuery('banks/').subscribe(
      res => {
        this.banks = res.response;
        this.errorLoading = false;
      }, error => {
        this.errorLoading = true;
        this.message = 'Ha sucedido algo, por favor vuelve a intentarlo.';
      }
    );
  }

  getCurrencies() {
    this.global.getQuery('currencies/').subscribe(
      res => {
        this.currencies = res.response;
        this.errorLoading = false;
      }, error => {
        this.errorLoading = true;
        this.message = 'Ha sucedido algo, por favor vuelve a intentarlo.';
      }
    );
  }

  ngOnSubmit() {
    this.global.postQuery('payment', this.form.value).subscribe(
      res => {
        this.message= 'Se ha registrado exitosamente el pago, por favor espere que el administrador lo apruebe';
        this.error = false;
      }, error => {
        this.error = true;
        this.message = 'Ha sucedido algo, por favor vuelve a intentarlo.';
      }
    );
  }

}
