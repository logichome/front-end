import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentReportComponent } from './payment-report/payment-report.component';
import {NgxSmartModalModule} from 'ngx-smart-modal';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [PaymentReportComponent],
  exports: [
    PaymentReportComponent
  ],
  imports: [
    CommonModule,
    NgxSmartModalModule,
    ReactiveFormsModule
  ]
})
export class UserModalsModule { }
