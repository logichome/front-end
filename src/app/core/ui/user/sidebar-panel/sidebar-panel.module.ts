import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import {RouterModule} from '@angular/router';
import {UserModalsModule} from '@ui/user/user-modals/user-modals.module';




@NgModule({
  declarations: [SidebarComponent],
  exports: [
    SidebarComponent
  ],
    imports: [
        CommonModule,
        RouterModule,
        UserModalsModule,

    ]
})
export class SidebarPanelModule { }
