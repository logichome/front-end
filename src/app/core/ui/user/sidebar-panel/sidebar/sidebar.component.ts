import {Component, Input, OnInit} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() residenceId: number;

  constructor(
    public modalService: NgxSmartModalService,
  ) {
  }

  ngOnInit(): void {
  }

}
